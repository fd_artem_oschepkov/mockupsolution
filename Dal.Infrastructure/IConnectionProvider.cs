﻿namespace Dal.Infrastructure
{
    public interface IConnectionProvider
    {
        string GetConnectionString();
    }
}