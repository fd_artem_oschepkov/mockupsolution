﻿namespace Dal.Infrastructure
{
    public interface ISessionFactory
    {
        IDbContext GetSession();
    }
}