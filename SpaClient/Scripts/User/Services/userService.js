﻿function UserService() {

    var self = this;

    function getUser(id) {
        $.getJSON("/Admin/api/Users", { id: id }, function (allData) {
            callback(allData);
        });
    };

    self.addUser = function (model, callback) {
        var user = model;
        $.ajax({
            type: "PUT",
            data: JSON.stringify(user),
            url: "/Admin/api/Users",
            contentType: "application/json"
        }).done(function (allData) {
            callback(allData);
        });
    };

    self.updateUser = function (model, callback) {
        var user = model;
        $.ajax({
            type: "POST",
            data: JSON.stringify(user),
            url: "/Admin/api/Users",
            contentType: "application/json"
        }).done(function (allData) {
            callback(allData);
        });
    };

    self.deleteUser = function (id, callback) {
        $.ajax({
            type: "DELETE",
            url: "/Admin/api/Users/" + id,
            contentType: "application/json"
        }).done(function (allData) {
            callback(allData);
        });
    };

    self.loadUsers = function (value, callback) {
        $.getJSON("/Admin/api/Users", { value: value }, function (allData) {
            callback(allData);
        });
    };

    self.loadOptions = function (callback) {
        $.getJSON("/Admin/api/Options", function (allData) {
            callback(allData);
        });
    };
}