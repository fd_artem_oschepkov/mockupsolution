﻿$(function () {

    initValidation();
    initBindingHandlers();

    var service = new UserService();
    var eventAggregator = new ko.subscribable();

    var usersViewModel = new UsersViewModel(service, eventAggregator);
    var userViewModel = new UserViewModel(service, eventAggregator);

    ko.applyBindings(usersViewModel, $("#userRegion")[0]);
    ko.applyBindings(userViewModel, $("#editRegion")[0]);

    // todo 
    eventAggregator.subscribe(function (newValue) {
        $("#editRegion").modal("hide");
    }, this, events.userWasCreated);

});