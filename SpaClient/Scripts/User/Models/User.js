﻿function User() {
    var self = this;

    self.Id = ko.observable(0);
    self.Name = ko.observable("").extend({ required: true });
    self.LastName = ko.observable("").extend({ required: true });
    self.Status = ko.observable(0);
    self.Roles = ko.observableArray([]);
    self.Pages = ko.observableArray([]);

    self.isNew = function() {
        return self.Id() == 0;
    }
}