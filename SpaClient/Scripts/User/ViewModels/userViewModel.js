﻿function UserViewModel(service, eventAggregator) {
    var self = this;

    self.user = ko.observable();
    self.statuses = ko.observableArray([]);
    self.roles = ko.observableArray([]);
    self.pages = ko.observableArray([]);

    self.isNew = ko.observable();
    self.header = ko.observable();

    self.errors = null;

    eventAggregator.subscribe(function (newValue) {
        fillViewModel(newValue);
    }, this, events.userEditing);

    self.edit = function () {
        self.errors = ko.validation.group(self.user());
        if (isValid()) {
            var model = createUser();
            if (self.user().isNew()) {
                service.addUser(model, function (allData) {
                    eventAggregator.notifySubscribers(allData, events.userWasCreated);
                });
            } else {
                service.updateUser(model, function (allData) {
                    eventAggregator.notifySubscribers(allData, events.userWasUpdated);
                });
            }
        } else {
            self.errors.showAllMessages();
        }
    };

    self.delete = function () {

        if (!confirm("Do you want to remove this user?")) {
            return;
        }

        var id = self.user().Id();
        service.deleteUser(id, function () {
            eventAggregator.notifySubscribers(id, events.userWasDeleted);
        });
    }

    function loadOptions() {
        service.loadOptions(function (allData) {
            self.statuses(allData.Statuses);
            self.roles(allData.Roles);
            self.pages(allData.Pages);
        });
    };

    function fillViewModel(model) {

        if (model.isNew()) {
            self.isNew = true;
            self.header("Create");
        } else {
            self.isNew = false;
            self.header("Edit");
        }

        self.user(model);
    }

    function createUser() {
        var model = {
            Id: self.user().Id(),
            Name: self.user().Name(),
            LastName: self.user().LastName(),
            Status: self.user().Status(),
            Roles: self.user().Roles(),
            Pages: self.user().Pages()
        }

        return model;
    }

    function isValid() {
        return self.errors().length == 0;
    }

    loadOptions();
}