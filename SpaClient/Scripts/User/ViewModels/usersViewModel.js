﻿function UsersViewModel(service, eventAggregator) {
    var self = this;

    self.users = ko.observableArray([]);
    self.selectedUser = new User();
    self.keyword = ko.observable();

    self.search = function (d, e) {
        if (e.keyCode === 13) {
            loadUsers();
            return false;
        }
        return true;
    };

    self.editUser = function (user) {
        self.selectedUser.Id(user.Id);
        self.selectedUser.Name(user.Name);
        self.selectedUser.LastName(user.LastName);
        self.selectedUser.Status(user.Status.Id);
        self.selectedUser.Roles(extractIds(user.Roles));
        self.selectedUser.Pages(extractIds(user.Pages));

        eventAggregator.notifySubscribers(self.selectedUser, events.userEditing);
    };

    self.deleteUser = function (user) {

        if (!confirm("Do you want to remove this user?")) {
            return;
        }

        var id = user.Id;
        service.deleteUser(id, function () {
            removeUser(id);
        });
    }

    self.createUser = function () {
        self.selectedUser = new User();
        eventAggregator.notifySubscribers(self.selectedUser, events.userEditing);
    };

    eventAggregator.subscribe(function (newValue) {
        self.users.push(newValue);
    }, this, events.userWasCreated);

    eventAggregator.subscribe(function (newValue) {
        var model = ko.utils.arrayFirst(self.users(), function (item) {
            return item.Id == newValue.Id;
        });

        var index = self.users.indexOf(model);
        self.users.remove(model);
        self.users.splice(index, 0, newValue);

    }, this, events.userWasUpdated);

    eventAggregator.subscribe(function (id) {
        removeUser(id);
    }, this, events.userWasDeleted);

    function removeUser(id) {
        self.users.remove(function (item) {
            return item.Id == id;
        });
    }

    function loadUsers() {
        service.loadUsers(self.keyword(), function (allData) {
            self.users(allData);
        });
    };

    function extractIds(collection) {
        var ids = ko.utils.arrayMap(collection, function (item) {
            return item.Id;
        });

        return ids;
    }

    loadUsers();
}