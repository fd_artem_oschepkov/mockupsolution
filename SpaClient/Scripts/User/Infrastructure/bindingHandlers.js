﻿function initBindingHandlers() {
    ko.bindingHandlers.showModal = {
        init: function() {
        },
        update: function(element, valueAccessor) {
            var value = valueAccessor();
            if (ko.utils.unwrapObservable(value)) {
                $(element).modal('show');
                $("input", element).focus();
            } else {
                $(element).modal('hide');
            }
        }
    };
}

