namespace SpaClient.Areas.Admin
{
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Routing;

    internal static class WebApiConfig
    {
        internal static void RegisterRoutes(AreaRegistrationContext context)
        {
            context.MapHttpRoute(
                "Admin_default_web_api",
                "Admin/api/{controller}/{id}",
                new { id = RouteParameter.Optional });

            // context.MapHttpRoute("Admin_default_web_api_action", "Admin/api/{controller}/{action}");
        }
    }

    public static class AreaRegistrationContextExtensions
    {
        public static Route MapHttpRoute(this AreaRegistrationContext context, string name, string routeTemplate)
        {
            return context.MapHttpRoute(name, routeTemplate, null, null);
        }

        public static Route MapHttpRoute(
            this AreaRegistrationContext context,
            string name,
            string routeTemplate,
            object defaults)
        {
            return context.MapHttpRoute(name, routeTemplate, defaults, null);
        }

        public static Route MapHttpRoute(
            this AreaRegistrationContext context,
            string name,
            string routeTemplate,
            object defaults,
            object constraints)
        {
            var route = context.Routes.MapHttpRoute(name, routeTemplate, defaults, constraints);
            if (route.DataTokens == null)
            {
                route.DataTokens = new RouteValueDictionary();
            }

            route.DataTokens.Add("area", context.AreaName);
            return route;
        }
    }
}