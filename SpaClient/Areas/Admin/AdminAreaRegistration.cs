﻿namespace SpaClient.Areas.Admin
{
    using System.Web.Mvc;
    using System.Web.Optimization;

    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            RegisterRoutes(context);
            RegisterBundles();
        }

        private static void RegisterRoutes(AreaRegistrationContext context)
        {
            WebApiConfig.RegisterRoutes(context);
            RouteConfig.RegisterRoutes(context);
        }

        private static void RegisterBundles()
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}