﻿namespace SpaClient.Areas.Admin.Controllers
{
    using System.Collections.ObjectModel;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using BL.Infrastructure;

    using Dto.User.Read;
    using Dto.User.Write;

    using SpaClient.Areas.Admin.Models;

    public class UsersController : ApiController
    {
        private readonly IUserBl userBl;

        public UsersController(IUserBl userBl)
        {
            this.userBl = userBl;
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            ReadOnlyCollection<UserOverview> models = this.userBl.GetOverviews(string.Empty);

            return Request.CreateResponse(HttpStatusCode.OK, models);
        }

        [HttpGet]
        public HttpResponseMessage Get(string value)
        {
            ReadOnlyCollection<UserOverview> models = this.userBl.GetOverviews(value);
            return Request.CreateResponse(HttpStatusCode.OK, models);
        }

        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            UserForEdit dto = this.userBl.Get(id);

            var model = new User(dto.Id, dto.Name, dto.LastName, dto.Status, dto.Roles, dto.Pages);

            return Request.CreateResponse(HttpStatusCode.OK, model);
        }

        [HttpPost]
        public HttpResponseMessage Post(User user)
        {
            if (!this.ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dto = new UserForUpdate(user.Id, user.Name, user.LastName, user.Status, user.Roles, user.Pages);
            this.userBl.Update(dto);
            UserOverview overview = this.userBl.GetOverview(dto.Id);

            return Request.CreateResponse(HttpStatusCode.Accepted, overview);
        }

        [HttpPut]
        public HttpResponseMessage Put([FromBody] User user)
        {
            if (!this.ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            var dto = new UserForCreate(user.Name, user.LastName, user.Status, user.Roles, user.Pages);
            long id = this.userBl.Add(dto);
            UserOverview overview = this.userBl.GetOverview(id);

            return Request.CreateResponse(HttpStatusCode.Created, overview);
        }

        public HttpResponseMessage Delete(int id)
        {
            this.userBl.Delete(id);
            return Request.CreateResponse(HttpStatusCode.OK);
        }
    }
}