namespace SpaClient.Areas.Admin.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;

    using BL.Infrastructure;

    using Domain.User.Enums;

    using Dto.Infrastructure;
    using Dto.User.Read;

    using OptionAggregate = SpaClient.Areas.Admin.Models.OptionAggregate;

    public class OptionsController : ApiController
    {
        private readonly IPageBl pageBl;

        private readonly IRoleBl roleBl;

        public OptionsController(IPageBl pageBl, IRoleBl roleBl)
        {
            this.pageBl = pageBl;
            this.roleBl = roleBl;
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            ReadOnlyCollection<ItemOverview> pages = this.pageBl.GetOverviews();
            ReadOnlyCollection<ItemOverview> roles = this.roleBl.GetOverviews();

            List<ItemOverview> statusesList =
                (from Enum d in Enum.GetValues(new Statuses().GetType())
                 select new ItemOverview(Convert.ToInt64(d), d.ToString())).ToList();

            var statuses = new ReadOnlyCollection<ItemOverview>(statusesList);
            var optionAggregate = new OptionAggregate(pages, roles, statuses);

            return this.Request.CreateResponse(HttpStatusCode.OK, optionAggregate);
        }
    }
}