﻿namespace SpaClient.Areas.Admin.Models
{
    using System.Collections.ObjectModel;

    using Dto.Infrastructure;

    public class OptionAggregate
    {
        public OptionAggregate(
            ReadOnlyCollection<ItemOverview> pages,
            ReadOnlyCollection<ItemOverview> roles,
            ReadOnlyCollection<ItemOverview> statuses)
        {
            this.Pages = pages;
            this.Roles = roles;
            this.Statuses = statuses;
        }

        public ReadOnlyCollection<ItemOverview> Pages { get; set; }

        public ReadOnlyCollection<ItemOverview> Roles { get; set; }

        public ReadOnlyCollection<ItemOverview> Statuses { get; set; }
    }
}