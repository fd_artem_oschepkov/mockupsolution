﻿namespace SpaClient.Areas.Admin.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public User()
        {
        }

        public User(long id, string name, string lastName, int status, List<long> roles, List<long> pages)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Status = status;
            this.Roles = roles;
            this.Pages = pages;
        }

        public long Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public int Status { get; set; }

        public List<long> Roles { get; set; }

        public List<long> Pages { get; set; }
    }
}