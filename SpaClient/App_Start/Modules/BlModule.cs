﻿namespace SpaClient.Modules
{
    using Autofac;

    using BL;
    using BL.Infrastructure;

    public class BlModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserBl>().As<IUserBl>().InstancePerRequest();
            builder.RegisterType<PageBl>().As<IPageBl>().InstancePerRequest();
            builder.RegisterType<RoleBl>().As<IRoleBl>().InstancePerRequest();

            base.Load(builder);
        }
    }
}