﻿namespace SpaClient.Modules
{
    using Autofac;

    using Dal;
    using Dal.Infrastructure;

    public class DataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ConnectionProvider>().As<IConnectionProvider>().InstancePerRequest();
            builder.RegisterType<MockupSessionFactory>().As<ISessionFactory>().InstancePerRequest();
            builder.RegisterType<UnitOfWorkFactory>().As<IUnitOfWorkFactory>().InstancePerRequest();

            base.Load(builder);
        }
    }
}