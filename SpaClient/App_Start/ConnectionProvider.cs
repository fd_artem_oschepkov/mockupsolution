﻿namespace SpaClient
{
    using System;
    using System.Configuration;
    using System.IO;

    using Dal.Infrastructure;

    public class ConnectionProvider: IConnectionProvider
    {
        public string GetConnectionString()
        {
            ConnectionStringSettings c = ConfigurationManager.ConnectionStrings["MockupContext"];
            if (c == null)
            {
                throw new Exception(); // todo
            }

           string fixedConnectionString = c.ConnectionString.Replace("{AppDir}", AppDomain.CurrentDomain.BaseDirectory);

            string combineString = Path.Combine(
                AppDomain.CurrentDomain.BaseDirectory,
                c.ConnectionString.Replace("{AppDir}", string.Empty));

            return fixedConnectionString;
        }
    }
}
