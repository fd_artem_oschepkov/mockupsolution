namespace SpaClient
{
    using System.Web.Optimization;

    internal static class BundleConfig
    {
        internal static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(
                new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery.validate.min.js",
                    "~/Scripts/jquery.validate.unobtrusive.min.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/bootstrap").Include(
                    "~/Scripts/bootstrap.min.js",
                    "~/Scripts/bootstrap-multiselect.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include("~/Scripts/modernizr-{version}.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/users").Include(
                    "~/Scripts/User/Infrastructure/*.js",
                    "~/Scripts/User/Models/*.js",
                    "~/Scripts/User/Services/*.js",
                    "~/Scripts/User/ViewModels/*.js",
                    "~/Scripts/User/controller.js"));

            bundles.Add(
                new ScriptBundle("~/bundles/knockout").Include(
                    "~/Scripts/knockout-{version}.js",
                    "~/Scripts/knockout.mapping-latest.js",
                    "~/Scripts/knockout.contextmenu.js",
                    "~/Scripts/knockout.validation.js"));

            bundles.Add(
                new StyleBundle("~/Content/css").Include(
                    "~/Content/bootstrap.css",
                    "~/Content/Site.css",
                    "~/Content/bootstrap-multiselect.css",
                    "~/Content/knockout.contextmenu.css"));
        }
    }
}