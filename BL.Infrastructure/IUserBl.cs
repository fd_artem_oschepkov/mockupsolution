﻿namespace BL.Infrastructure
{
    using System.Collections.ObjectModel;

    using Dto.User.Read;
    using Dto.User.Write;

    public interface IUserBl
    {
        ReadOnlyCollection<UserOverview> GetOverviews(string searchString);

        UserForEdit Get(long id);

        UserOverview GetOverview(long id);

        long Add(UserForCreate dto);

        void Update(UserForUpdate dto);

        void Delete(long id);
    }
}
