﻿namespace BL.Infrastructure
{
    using System.Collections.ObjectModel;

    using Dto.Infrastructure;

    public interface IRoleBl
    {
        ReadOnlyCollection<ItemOverview> GetOverviews();
    }
}