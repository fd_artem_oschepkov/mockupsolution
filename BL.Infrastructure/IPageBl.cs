﻿namespace BL.Infrastructure
{
    using System.Collections.ObjectModel;

    using Dto.Infrastructure;

    public interface IPageBl
    {
        ReadOnlyCollection<ItemOverview> GetOverviews();
    }
}