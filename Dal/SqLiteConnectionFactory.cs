namespace Dal
{
    using System.Data.Common;
    using System.Data.Entity.Infrastructure;
    using System.Data.SQLite;

    public class SqLiteConnectionFactory : IDbConnectionFactory
    {
        public DbConnection CreateConnection(string nameOrConnectionString)
        {
            return new SQLiteConnection(nameOrConnectionString);
        }
    }
}