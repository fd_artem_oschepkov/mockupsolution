﻿namespace Dal
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Common;
    using System.Data.SQLite;
    using System.Data.SQLite.EF6;
    using System.Reflection;

    public class SqLiteConfiguration : DbConfiguration
    {
        public SqLiteConfiguration()
        {
            this.SetDefaultConnectionFactory(new SqLiteConnectionFactory());
            this.SetProviderFactory("System.Data.SQLite", SQLiteFactory.Instance);
            this.SetProviderFactory("System.Data.SQLite.EF6", SQLiteProviderFactory.Instance);
            Type type = Type.GetType("System.Data.SQLite.EF6.SQLiteProviderServices, System.Data.SQLite.EF6");

            // todo reflection is needed unlike the SQL Server and SQL Server CE providers which are public
            if (type != null)
            {
                FieldInfo fieldInfo = type.GetField("Instance", BindingFlags.NonPublic | BindingFlags.Static);
                if (fieldInfo != null)
                {
                    this.SetProviderServices("System.Data.SQLite", (DbProviderServices)fieldInfo.GetValue(null));
                }
            }
        }
    }
}