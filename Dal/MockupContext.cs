﻿namespace Dal
{
    using System.Data.Entity;
    using System.Data.SQLite;

    using Dal.Infrastructure;

    using Domain.User;

    public class MockupContext : DbContext, IDbContext
    {
        public MockupContext(string connectionString)
            : base(
                new SQLiteConnection()
                    {
                        ConnectionString =
                            new SQLiteConnectionStringBuilder()
                                {
                                    DataSource = connectionString,
                                    ForeignKeys = true
                                }
                            .ConnectionString
                    },
                true)
        {
            Database.SetInitializer<MockupContext>(null);
        }

        public DbSet<User> User { get; set; }

        public DbSet<Page> Pages { get; set; }

        public DbSet<Role> Roles { get; set; }
    }
}
