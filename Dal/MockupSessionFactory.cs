﻿namespace Dal
{
    using Dal.Infrastructure;

    public class MockupSessionFactory : ISessionFactory
    {
        private readonly IConnectionProvider connectionProvider;

        public MockupSessionFactory(IConnectionProvider connectionProvider)
        {
            this.connectionProvider = connectionProvider;
        }

        public IDbContext GetSession()
        {
            return new MockupContext(this.connectionProvider.GetConnectionString());
        }
    }
}