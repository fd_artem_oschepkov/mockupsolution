﻿namespace Domain.Infrastructure
{
    public interface INamedEntity
    {
        string Name { get; set; }
    }
}