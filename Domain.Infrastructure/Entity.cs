﻿namespace Domain.Infrastructure
{
    using System;

    public abstract class Entity
    {
        public long Id { get; set; }
    }
}