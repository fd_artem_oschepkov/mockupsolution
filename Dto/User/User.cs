﻿namespace Dto.User
{
    using System;
    using System.Collections.Generic;

    public abstract class User : Infrastructure.Dto
    {
        protected User(string name, string lastName, int status, List<long> roles, List<long> pages)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Must not be null or empty", "name");
            }

            if (string.IsNullOrWhiteSpace(lastName))
            {
                throw new ArgumentException("Must not be null or empty", "lastName");
            }

            this.Name = name;
            this.LastName = lastName;
            this.Status = status;
            this.Roles = roles;
            this.Pages = pages;
        }

        public string Name { get; private set; }

        public string LastName { get; private set; }

        public int Status { get; private set; }

        public List<long> Roles { get; private set; }

        public List<long> Pages { get; private set; }
    }
}