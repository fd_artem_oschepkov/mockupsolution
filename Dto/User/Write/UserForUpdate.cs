﻿namespace Dto.User.Write
{
    using System.Collections.Generic;

    public class UserForUpdate : User
    {
        public UserForUpdate(long id, string name, string lastName, int status, List<long> roles, List<long> pages)
            : base(name, lastName, status, roles, pages)
        {
            this.Id = id;
        }
    }
}