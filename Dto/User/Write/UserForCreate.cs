﻿namespace Dto.User.Write
{
    using System.Collections.Generic;

    public class UserForCreate : User
    {
        public UserForCreate(string name, string lastName, int status, List<long> roles, List<long> pages)
            : base(name, lastName, status, roles, pages)
        {
        }
    }
}