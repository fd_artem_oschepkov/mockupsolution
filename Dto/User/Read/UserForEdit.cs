﻿namespace Dto.User.Read
{
    using System.Collections.Generic;

    public class UserForEdit : User
    {
        public UserForEdit(long id, string name, string lastName, int status, List<long> roles, List<long> pages)
            : base(name, lastName, status, roles, pages)
        {
            this.Id = id;
        }
    }
}