﻿namespace Dto.User.Read
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    using Dto.Infrastructure;

    [DataContract]
    public class UserOverview : Infrastructure.Dto
    {
        public UserOverview(
            long id,
            string name,
            string lastName,
            ItemOverview status,
            List<ItemOverview> roles,
            List<ItemOverview> pages,
            bool isAdmin)
        {
            this.Id = id;
            this.Name = name;
            this.LastName = lastName;
            this.Status = status;
            this.Roles = roles;
            this.Pages = pages;
            this.IsAdmin = isAdmin;
        }

        [DataMember]
        public string Name { get; private set; }

        [DataMember]
        public string LastName { get; private set; }

        [DataMember]
        public ItemOverview Status { get; private set; }

        [DataMember]
        public List<ItemOverview> Roles { get; private set; }

        [DataMember]
        public List<ItemOverview> Pages { get; private set; }

        [DataMember]
        public bool IsAdmin { get; private set; }
    }
}