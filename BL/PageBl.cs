namespace BL
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using BL.Infrastructure;

    using Dal.Infrastructure;

    using Domain.User;

    using Dto.Infrastructure;

    public class PageBl : IPageBl
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        public PageBl(IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public ReadOnlyCollection<ItemOverview> GetOverviews()
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<Page> pageRepository = uof.GetRepository<Page>();

                List<ItemOverview> overviews = pageRepository.GetAll().ToList().Select(x => new ItemOverview(x.Id, x.Name)).ToList();

                return new ReadOnlyCollection<ItemOverview>(overviews);
            }
        }
    }
}