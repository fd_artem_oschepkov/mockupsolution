﻿namespace BL
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using BL.Infrastructure;

    using Dal.Infrastructure;

    using Domain.User;

    using Dto.Infrastructure;

    public class RoleBl : IRoleBl
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        public RoleBl(IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public ReadOnlyCollection<ItemOverview> GetOverviews()
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<Role> roleRepository = uof.GetRepository<Role>();

                List<ItemOverview> overviews = roleRepository.GetAll().ToList().Select(x => new ItemOverview(x.Id, x.Name)).ToList();

                return new ReadOnlyCollection<ItemOverview>(overviews);
            }
        }
    }
}