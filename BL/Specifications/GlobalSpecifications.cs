﻿namespace BL.Specifications
{
    using System.Collections.Generic;
    using System.Linq;

    using Domain.Infrastructure;

    public static class GlobalSpecifications
    {
        public static IEnumerable<T> GetNamed<T>(this IEnumerable<T> enumerable, string searchString)
            where T : INamedEntity
        {
            string param = searchString.Trim().ToLower();

            IEnumerable<T> models = enumerable.Where(x => x.Name.Trim().ToLower().Contains(param));

            return models;
        }

        public static IEnumerable<T> GetById<T>(this IEnumerable<T> enumerable, long id) where T : Entity
        {
            IEnumerable<T> models = enumerable.Where(x => x.Id == id);

            return models;
        }
    }
}