﻿namespace BL.Specifications.User
{
    using System.Collections.Generic;
    using System.Linq;

    using Domain.User;

    public static class UserSpecifications
    {
        public static IEnumerable<User> GetBySearchUsers(this IEnumerable<User> enumerable, string searchString)
        {
            if (string.IsNullOrEmpty(searchString))
            {
                return enumerable;
            }

            string param = searchString.Trim().ToLower();

            IEnumerable<User> models =
                enumerable.Where(
                    x => x.Name.Trim().ToLower().Contains(param) || x.LastName.Trim().ToLower().Contains(param));

            return models;
        }
    }
}
