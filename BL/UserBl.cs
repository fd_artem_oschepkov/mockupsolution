﻿namespace BL
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    using BL.Infrastructure;
    using BL.Specifications;
    using BL.Specifications.User;

    using Dal.Infrastructure;

    using Domain.User;
    using Domain.User.Enums;

    using Dto.Infrastructure;
    using Dto.User.Read;
    using Dto.User.Write;

    public class UserBl : IUserBl
    {
        private readonly IUnitOfWorkFactory unitOfWorkFactory;

        public UserBl(IUnitOfWorkFactory unitOfWorkFactory)
        {
            this.unitOfWorkFactory = unitOfWorkFactory;
        }

        public ReadOnlyCollection<UserOverview> GetOverviews(string searchString)
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<User> userRepository = uof.GetRepository<User>();

                IEnumerable<User> anonymus =
                    userRepository.GetAll(x => x.Roles, x => x.Pages).GetBySearchUsers(searchString).ToList();

                List<UserOverview> overviews = anonymus.Select(GetUserOverview).ToList();

                return new ReadOnlyCollection<UserOverview>(overviews);
            }
        }

        public UserForEdit Get(long id)
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<User> userRepository = uof.GetRepository<User>();

                User entity = userRepository.GetAll(x => x.Roles, x => x.Pages).GetById(id).Single();

                var dto = new UserForEdit(
                    entity.Id,
                    entity.Name,
                    entity.LastName,
                    (int)entity.Status,
                    entity.Roles.Select(y => y.Id).ToList(),
                    entity.Pages.Select(y => y.Id).ToList());

                return dto;
            }
        }

        public UserOverview GetOverview(long id)
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<User> userRepository = uof.GetRepository<User>();

                User entity = userRepository.GetAll(x => x.Roles, x => x.Pages).GetById(id).Single();
                UserOverview dto = GetUserOverview(entity);

                return dto;
            }
        }

        public void Delete(long id)
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<User> userRepository = uof.GetRepository<User>();

                User entity = userRepository.GetById(id);
                entity.Roles.Clear();
                entity.Pages.Clear();
                userRepository.DeleteById(id);

                uof.Save();
            }
        }

        public void Update(UserForUpdate dto)
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<User> userRepository = uof.GetRepository<User>();
                IRepository<Role> roleRepository = uof.GetRepository<Role>();
                IRepository<Page> pageRepository = uof.GetRepository<Page>();

                User entity = userRepository.Find(x => x.Id == dto.Id, x => x.Roles, x => x.Pages).Single();
                Map(entity, dto);

                foreach (var role in entity.Roles.ToList())
                {
                    if (dto.Roles == null || !dto.Roles.Contains(role.Id))
                    {
                        entity.Roles.Remove(role);
                    }
                }

                if (dto.Roles != null && dto.Roles.Any())
                {
                    foreach (var newId in dto.Roles)
                    {
                        if (entity.Roles.All(r => r.Id != newId))
                        {
                            var role = new Role { Id = newId };

                            roleRepository.Attach(role);
                            entity.Roles.Add(role);
                        }
                    }
                }

                // todo refactoring
                foreach (var page in entity.Pages.ToList())
                {
                    if (dto.Pages == null || !dto.Pages.Contains(page.Id))
                    {
                        entity.Pages.Remove(page);
                    }
                }

                if (dto.Pages != null && dto.Pages.Any())
                {
                    foreach (var newId in dto.Pages)
                    {
                        if (entity.Pages.All(r => r.Id != newId))
                        {
                            var page = new Page { Id = newId };

                            pageRepository.Attach(page);
                            entity.Pages.Add(page);
                        }
                    }
                }

                uof.Save();
            }
        }

        public long Add(UserForCreate dto)
        {
            using (IUnitOfWork uof = this.unitOfWorkFactory.Create())
            {
                IRepository<User> userRepository = uof.GetRepository<User>();
                IRepository<Role> roleRepository = uof.GetRepository<Role>();
                IRepository<Page> pageRepository = uof.GetRepository<Page>();

                var entity = new User();

                Map(entity, dto);

                if (dto.Roles != null && dto.Roles.Any())
                {
                    foreach (var id in dto.Roles)
                    {
                        var program = new Role { Id = id };

                        roleRepository.Attach(program);
                        entity.Roles.Add(program);
                    }
                }

                if (dto.Pages != null && dto.Pages.Any())
                {
                    foreach (var id in dto.Pages)
                    {
                        var page = new Page { Id = id };

                        pageRepository.Attach(page);
                        entity.Pages.Add(page);
                    }
                }

                userRepository.Add(entity);

                uof.Save();

                return entity.Id;
            }
        }

        #region Private

        private static UserOverview GetUserOverview(User user)
        {
            return new UserOverview(
                user.Id,
                user.Name,
                user.LastName,
                new ItemOverview((int)user.Status, user.Status.ToString()),
                user.Roles.Select(y => new ItemOverview(y.Id, y.Name)).ToList(),
                user.Pages.Select(y => new ItemOverview(y.Id, y.Name)).ToList(),
                user.Roles.Any(y => y.RoleId == RoleIds.Admin));
        }

        private static void Map(User domain, global::Dto.User.User dto)
        {
            domain.Name = dto.Name;
            domain.LastName = dto.LastName;
            domain.Status = (Statuses)dto.Status;
        }

        #endregion
    }
}
