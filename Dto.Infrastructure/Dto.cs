﻿namespace Dto.Infrastructure
{
    using System.Runtime.Serialization;

    [DataContract]
    public abstract class Dto
    {
        [DataMember]
        public long Id { get; protected set; }
    }
}