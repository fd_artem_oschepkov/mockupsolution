﻿namespace Dto.Infrastructure
{
    using System.Runtime.Serialization;

    [DataContract]
    public class ItemOverview : Dto
    {
        public ItemOverview(long id, string name)
        {
            this.Id = id;
            this.Name = name;
        }

        [DataMember]
        public string Name { get; private set; }
    }
}