﻿namespace Domain.User
{
    using System.Collections.Generic;

    using Domain.Infrastructure;
    using Domain.User.Enums;

    public class Role : Entity, INamedEntity
    {
        public string Name { get; set; }

        public RoleIds RoleId { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}