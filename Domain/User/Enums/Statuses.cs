﻿namespace Domain.User.Enums
{
    public enum Statuses
    {
        Single = 0,

        Married = 1,

        Divorced = 2
    }

    public enum RoleIds
    {
        Admin = 1,
    }
}