﻿namespace Domain.User
{
    using System.Collections.Generic;

    using Domain.Infrastructure;
    using Domain.User.Enums;

    public class User : Entity, INamedEntity
    {
        public User()
        {
            // todo
            this.Roles = new List<Role>();
            this.Pages = new List<Page>();
        }

        public string Name { get; set; }

        public string LastName { get; set; }

        public Statuses Status { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        public virtual ICollection<Page> Pages { get; set; }
    }
}