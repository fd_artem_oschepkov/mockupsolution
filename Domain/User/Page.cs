﻿namespace Domain.User
{
    using System.Collections.Generic;

    using Domain.Infrastructure;

    public class Page : Entity, INamedEntity
    {
        public string Name { get; set; }

        public virtual ICollection<User> Users { get; set; }
    }
}